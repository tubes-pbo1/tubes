proses bisnis dari program Gojek yang telah diberikan:

//    1. User Authentication

//     User memasukkan nama, password, dan nomor HP.
//    Sistem memeriksa kecocokan data pengguna dengan data yang ada di sistem Gojek.
//    Jika data pengguna sesuai, maka sistem menampilkan pesan "Selamat Datang Di Gojek" dan menu utama.
//    Jika data pengguna tidak sesuai, maka sistem meminta pengguna untuk memasukkan data ulang atau mendaftar akun baru.

//   2. Gojek Service
//   Pengguna memilih menu Gojek.
//   Memilih driver yang ready di sekitar
//   Pengguna memasukkan jarak tempuh.
//   Sistem menghitung total harga dan menampilkan detail pesanan.
//   Sistem menampilkan pesan "Pembayaran sebesar [jumlah uang] berhasil dilakukan dengan [metode pembayaran]."

// 3. GoFood Service
//  Pengguna memilih menu GoFood.
//  Sistem menampilkan daftar menu makanan yang tersedia.
//  Pengguna memilih menu makanan yang ingin dipesan.
//  Pengguna memasukkan jarak tempuh.
//  Sistem menghitung total harga dan menampilkan detail pesanan.
//  Pengguna memilih metode pembayaran.
//  Sistem memproses pembayaran.
//  Sistem menampilkan pesan "Pembayaran sebesar [jumlah uang] berhasil dilakukan dengan [metode pembayaran]."
//  Order

// order history
// Pengguna memilih menu Order History.
// Sistem menampilkan riwayat pesanan pengguna, termasuk detail pesanan dan informasi pembayaran.

// Payment
// Pengguna memilih menu Payment.
// Sistem menampilkan pilihan metode pembayaran.
// Pengguna memilih metode pembayaran.
// Pengguna memasukkan jumlah uang yang ingin dibayarkan.
// Sistem memproses pembayaran.
// Sistem menampilkan pesan "Pembayaran sebesar [jumlah uang] berhasil dilakukan dengan [metode pembayaran]."
