# 1.Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:
use case User

use case|Deskripsi|nilai
-------|----|------
Register Akun | Penguna dapat Mendaftar Akun|Tinggi
login | Pengguna Dapat Melakukan Login | Tinggi
Pemesanan Transportasi | Pengguna dapat memilih layanan Goride Gocar GoBlubird Gosend Gotransit Gobox| Tinggi
Pemesanan Layanan Makanan | Pengguna dapat memesan Gofood|Tinggi
Pemesanan layanan Kesehatan | Pengguna Dapat memesan Obat di Gomed | sedang
Pemesanan layanan belanja | Pengguna dapat belanja melalui Gomart Goshop| sedang 
Pemesanan layanan tagihan | pengguna dapat membayar tagihan melalui Gotagihan | sedang
Pembayaran Digital| Pengguna dapat melakukan pembayaran digital yang di gunakan| sedang
Penilaian dan Ulasan | Setelah selesai menggunakan layanan, penguna dapat memberikan penilaian dan ulasan | sedang
Riwayat Transaksi | pengguna dapat melihat riwayat Transaksi dan detail Transaksi sebelumnya| sedang 
Bantuan dan Dukungan | penguna dapat mengakses bantuan dan dukungan jika mengalami masalah| sedang 


use case manajemen perusahaan 

use case|Deskripsi|nilai
-------|----|------
manajemen Layanan|perusahaan dapat mengelola dan mengatrur berbagai layanan yang disediakan, seperti Transportasi, pengiriman Makanan dan lainnya|sedang
manajemen Mitra | perusahaan dapat mengelola mitra pengemudi pengantar makanan yang terdaftar di gojek | sedang
pembayaran mitra | perusahaan dapat melakuakn pembayaran ke pengemudi yang melakukan tugas nya| sedang
Analisis dan pemantauan kinerja| perusahaan dapat melakukan Analisis dan pemantauan terhadap kinerja layanan, mitra , dan Transaksi untuk pengambilan keputusan yang lebih baik| sedang
Manajemen Pelanggan	|Perusahaan dapat mengelola data pelanggan, termasuk informasi akun, riwayat transaksi, dan preferensi pengguna.| sedang
Penanganan Keluhan Pelanggan |Perusahaan dapat menangani keluhan pelanggan dan memberikan solusi yang tepat.| sedang
Manajemen Promosi dan Diskon|Perusahaan dapat mengelola program promosi, diskon, dan penawaran khusus untuk menarik pelanggan dan meningkatkan penggunaan layanan|sedang
Analisis dan Perencanaan Ekspansi|Perusahaan dapat melakukan analisis pasar dan perencanaan ekspansi ke wilayah baru berdasarkan data dan tren yang ada| sedang

Use Case Direksi Perusahaan Gojek (Dashboard, Monitoring, Analisis):

use case|Deskripsi|nilai
-------|----|------
Dashboard Pencapaian|Direksi dapat melihat dashboard yang memberikan informasi seputar pencapaian kinerja perusahaan, termasuk transaksi, pendapatan, dan pengguna baru|sedang
Analisis Kinerja Layanan|Direksi dapat menganalisis kinerja layanan Gojek, termasuk penggunaan layanan transportasi dan pengiriman makanan di berbagai wilayah|sedang
Pemantauan Pelanggan|Direksi dapat memantau data pelanggan, termasuk pertumbuhan pengguna baru, preferensi pengguna, dan tingkat kepuasan pelanggan| sedang
Analisis Mitra Pengemudi dan Pengantar Makanan|Direksi dapat menganalisis kinerja mitra pengemudi dan pengantar makanan, termasuk jumlah transaksi, rata-rata rating, dan efisiensi pengemudi|sedang
Analisis Keuangan|Direksi dapat melakukan analisis keuangan dan melihat laporan pendapatan, biaya, dan laba perusahaan|sedang
Pemantauan Persaingan|Direksi dapat memantau persaingan di industri layanan on-demand dan melihat tren pasar serta strategi pesaing|sedang
Pengambilan Keputusan Strategis|Direksi dapat menggunakan data dan analisis yang tersedia untuk mengambil keputusan strategis terkait pengembangan produk, perluasan pasar, dan investasi perusahaan|sedang



# 2.Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
## Diagram Class
```mermaid
classDiagram
AppGojek --* Akun
AppGojek : +ArrayList
AppGojek --* Goride
AppGojek --* Gofood
AppGojek --* Gomart
AppGojek --* Gotix
AppGojek --* Gocar
AppGojek --* GoTagihan
AppGojek --* GoTransit
AppGojek --* Gomed
AppGojek --* Gosend
AppGojek --* Gobox

Class Akun 
<<Abstract>> Akun 
Akun : +String Nama
Akun : +String Nomer Telpon
Akun : + intorductiomyself()
Akun --* User
User : +String Email
User : +String Pasword
User : +Login()
Akun --* Driver 
Driver : +String PlatNomber
Driver : +Login()
Class Goride
Goride : +Double Distance
Goride : +String Lokasi Awal
Goride : +String LokasiTujuan
Goride : +LokasiAwal()
Goride : +LokasiTujuan()
Goride --|> Order

Class Order
Order : +Double Distance
Order : +Double Price
Order : + String IteName
Order : +getPrice()

Class Gofood
Gofood --|> Order
Gofood : +String MenuMakanan
Gofood : +PesanMakanan()

Class Gomart
Gomart --|> Order

Class Payment
Payment --|> Order
Payment : +Double Amount
Payment : +String PaymentMethode
Payment : +setaAmount
Payment : +setPaymentMethode
```
# 3.Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
```java
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

class GojekApp extends JFrame {
    private JTextField usernameField;
    private JPasswordField passwordField;

    private List<Service> availableServices;
    private List<Order> orderHistory;
    private Order currentOrder;

    public GojekApp() {
        setTitle("GojekApps");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);
        setLocationRelativeTo(null);
        setLayout(new GridLayout(4, 2));

        // Gojek Colors
        Color gojekGreen = new Color(8, 190, 70);
        Color gojekBlue = new Color(9, 132, 227);

        // Initialize the list of available services
        availableServices = new ArrayList<>();
        initializeAvailableServices();

        orderHistory = new ArrayList<>();


        JPanel mainPanel = new JPanel(new BorderLayout());

        // Header panel (hijau)
        JPanel headerPanel = new JPanel();
        headerPanel.setBackground(gojekGreen);
        headerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        JLabel logoLabel = new JLabel();
        ImageIcon logoIcon = new ImageIcon("gojek.png"); // Make sure the icon file exists
        Image logoImage = logoIcon.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT);
        logoIcon = new ImageIcon(logoImage);
        logoLabel.setIcon(logoIcon);
        headerPanel.add(logoLabel);

        JLabel usernameLabel = new JLabel("Username:");
        usernameField = new JTextField();
        JLabel passwordLabel = new JLabel("Password:");
        passwordField = new JPasswordField();
        JButton loginButton = new JButton("Login");
        JButton registerButton = new JButton("Register");

        loginButton.setBackground(gojekGreen);
        loginButton.setForeground(Color.WHITE);
        loginButton.setFocusPainted(false);

        registerButton.setBackground(gojekBlue);
        registerButton.setForeground(Color.WHITE);
        registerButton.setFocusPainted(false);

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = usernameField.getText();
                String password = new String(passwordField.getPassword());
                login(username, password);
            }
        });

        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showRegisterPage();
            }
        });

        add(logoLabel);
        add(new JLabel());
        add(usernameLabel);
        add(usernameField);
        add(passwordLabel);
        add(passwordField);
        add(loginButton);
        add(registerButton);
    }

    public void showRegisterPage() {
        JFrame registerFrame = new JFrame();
        registerFrame.setTitle("Register");
        registerFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        registerFrame.setSize(300, 200);
        registerFrame.setLocationRelativeTo(null);
        registerFrame.setLayout(new GridLayout(3, 2, 10, 10));

        JLabel usernameLabel = new JLabel("Username:");
        JTextField usernameField = new JTextField();

        JLabel passwordLabel = new JLabel("Password:");
        JPasswordField passwordField = new JPasswordField();

        JButton registerButton = new JButton("Register");
        JButton cancelButton = new JButton("Cancel");

        registerButton.setBackground(new Color(8, 190, 70));
        registerButton.setForeground(Color.WHITE);
        registerButton.setFocusPainted(false);

        cancelButton.setBackground(new Color(9, 132, 227));
        cancelButton.setForeground(Color.WHITE);
        cancelButton.setFocusPainted(false);

        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = usernameField.getText();
                String password = new String(passwordField.getPassword());
                register(username, password);
                registerFrame.dispose();
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerFrame.dispose();
            }
        });

        registerFrame.add(usernameLabel);
        registerFrame.add(usernameField);
        registerFrame.add(passwordLabel);
        registerFrame.add(passwordField);
        registerFrame.add(registerButton);
        registerFrame.add(cancelButton);

        registerFrame.setVisible(true);
    }

    public void register(String username, String password) {
        JOptionPane.showMessageDialog(this, "Registered successfully!");
    }

    private void initializeAvailableServices() {
        availableServices.add(new Service("GoRide", "Ride", 10000.0, 5000.0));
        availableServices.add(new Service("GoFood", "Food", 0.0, 0.0));
        // Add more services as needed
    }

    public void login(String username, String password) {
        if (username.equals("admin") && password.equals("admin123")) {
            JOptionPane.showMessageDialog(this, "Login successful!");
            dispose();
            showMainPage();
        } else {
            JOptionPane.showMessageDialog(this, "Login failed!");
        }
    }

    public void showMainPage() {
        JFrame mainPage = new JFrame();
        mainPage.setTitle("Main Page");
        mainPage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainPage.setSize(400, 300);
        mainPage.setLocationRelativeTo(null);
        mainPage.setLayout(new BorderLayout());

        Color gojekGreen = new Color(8, 190, 70);
        Color gojekBlue = new Color(9, 132, 227);

        JPanel headerPanel = new JPanel();
        JLabel searchLabel = new JLabel("Search:");
        JTextField searchField = new JTextField(20);
        JButton searchButton = new JButton("Search");

        searchButton.setBackground(gojekBlue);
        searchButton.setForeground(Color.WHITE);
        searchButton.setFocusPainted(false);

        headerPanel.setBackground(gojekGreen);
        headerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        headerPanel.add(searchLabel);
        headerPanel.add(searchField);
        headerPanel.add(searchButton);

        JPanel servicesPanel = new JPanel();
        servicesPanel.setLayout(new GridLayout(2, 2, 10, 10));

        for (Service service : availableServices) {
            JButton serviceButton = new JButton(service.getServiceName());
            serviceButton.setBackground(gojekBlue);
            serviceButton.setForeground(Color.WHITE);
            serviceButton.setFocusPainted(false);

            serviceButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (currentOrder != null) {
                        JOptionPane.showMessageDialog(null, "You already have an active order: " + currentOrder.getServiceType());
                        return;
                    }

                    handleServiceSelection(service);
                }
            });

            servicesPanel.add(serviceButton);
        }

        mainPage.add(headerPanel, BorderLayout.NORTH);
        mainPage.add(servicesPanel, BorderLayout.CENTER);

        // Add the Order History button to the main page
        JButton orderHistoryButton = new JButton("Order History");
        orderHistoryButton.setBackground(gojekBlue);
        orderHistoryButton.setForeground(Color.WHITE);
        orderHistoryButton.setFocusPainted(false);

        orderHistoryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showOrderHistory();
            }
        });

        mainPage.add(orderHistoryButton, BorderLayout.SOUTH);

        mainPage.setVisible(true);
    }

    private void handleServiceSelection(Service service) {
        if (service.getServiceType().equals("Ride")) {
            List<Driver> availableDrivers = getAvailableDrivers();
            showAvailableDrivers(availableDrivers);
        } else if (service.getServiceType().equals("Food")) {
            // Implement GoFood service here
            // Add logic for food delivery service
            JOptionPane.showMessageDialog(null, "GoFood service is under development.");
        }
        // Add more cases for other services as needed
    }

    public void showAvailableDrivers(List<Driver> drivers) {
        JFrame driversFrame = new JFrame();
        driversFrame.setTitle("Available Drivers");
        driversFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        driversFrame.setSize(300, 200);
        driversFrame.setLocationRelativeTo(null);
        driversFrame.setLayout(new GridLayout(drivers.size() + 1, 1));

        ButtonGroup driverGroup = new ButtonGroup();

        for (Driver driver : drivers) {
            JRadioButton driverButton = new JRadioButton(driver.getName() + " - " + driver.getLicensePlate());
            driverButton.setActionCommand(driver.getLicensePlate());
            driverButton.setBackground(Color.WHITE);
            driverGroup.add(driverButton);
            driversFrame.add(driverButton);
        }

        JButton selectButton = new JButton("Select");
        Color gojekBlue = new Color(5, 152, 226);
        selectButton.setForeground(Color.WHITE);
        selectButton.setFocusPainted(false);
        driversFrame.add(selectButton);

        selectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedLicensePlate = driverGroup.getSelection().getActionCommand();
                driversFrame.dispose();
                Driver selectedDriver = getDriverByLicensePlate(drivers, selectedLicensePlate);
                if (selectedDriver != null) {
                    JOptionPane.showMessageDialog(null, "Selected driver: " + selectedDriver.getName());
                    selectLocation(selectedDriver);
                }
            }
        });

        driversFrame.setVisible(true);
    }

    private List<Driver> getAvailableDrivers() {
        List<Driver> drivers = new ArrayList<>();
        drivers.add(new Driver("John", "ABC123"));
        drivers.add(new Driver("Sarah", "XYZ789"));
        return drivers;
    }

    private Driver getDriverByLicensePlate(List<Driver> drivers, String licensePlate) {
        for (Driver driver : drivers) {
            if (driver.getLicensePlate().equals(licensePlate)) {
                return driver;
            }
        }
        return null;
    }

    private void selectLocation(Driver driver) {
        String startLocation = JOptionPane.showInputDialog("Enter start location:");
        String destination = JOptionPane.showInputDialog("Enter destination:");
        double distance = Double.parseDouble(JOptionPane.showInputDialog("Enter distance in km:"));

        double fare = availableServices.get(0).getBaseFare() + (distance * availableServices.get(0).getRatePerKilometer());

        currentOrder = new Order("GoRide", driver, startLocation, destination, distance, fare);

        String[] paymentMethods = {"Credit Card", "Cash"};
        String selectedPaymentMethod = (String) JOptionPane.showInputDialog(null,
                "Choose payment method:", "Payment Method",
                JOptionPane.QUESTION_MESSAGE, null, paymentMethods, paymentMethods[0]);

        if (selectedPaymentMethod != null) {
            currentOrder.setPaymentMethod(selectedPaymentMethod);
            JOptionPane.showMessageDialog(null, "Order placed!\nDriver: " + driver.getName() + "\nFare: " + fare
                    + "\nPayment Method: " + selectedPaymentMethod);

            orderHistory.add(currentOrder);
        }
    }

    public void showOrderHistory() {
        JFrame orderHistoryFrame = new JFrame();
        orderHistoryFrame.setTitle("Order History");
        orderHistoryFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        orderHistoryFrame.setSize(600, 400);
        orderHistoryFrame.setLocationRelativeTo(null);

        String[] columnNames = {"Service Type", "Driver/Restaurant", "Start Location", "Destination/Item", "Distance/Price", "Fare/Payment"};
        Object[][] data = new Object[orderHistory.size()][6];

        for (int i = 0; i < orderHistory.size(); i++) {
            Order order = orderHistory.get(i);
            if (order.getServiceType().equals("GoRide")) {
                data[i][0] = order.getServiceType();
                data[i][1] = order.getDriver().getName();
                data[i][2] = order.getStartLocation();
                data[i][3] = order.getDestination();
                data[i][4] = order.getDistance() + " km";
                data[i][5] = "IDR " + order.getFare();
            } else if (order.getServiceType().equals("GoFood")) {
                data[i][0] = order.getServiceType();
                data[i][1] = order.getRestaurantName();
                data[i][2] = order.getStartLocation();
                data[i][3] = order.getDestination();
                data[i][4] = "Food Delivery";
                data[i][5] = "IDR " + order.getFare();
            }
        }
        JTable orderHistoryTable = new JTable(data, columnNames);
        JScrollPane scrollPane = new JScrollPane(orderHistoryTable);
        orderHistoryFrame.add(scrollPane);

        orderHistoryFrame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                GojekApp gojekApp = new GojekApp();
                gojekApp.setVisible(true);
            }
        });
    }
}

class Service {
    private String serviceName;
    private String serviceType;
    private double baseFare;
    private double ratePerKilometer;

    public Service(String serviceName, String serviceType, double baseFare, double ratePerKilometer) {
        this.serviceName = serviceName;
        this.serviceType = serviceType;
        this.baseFare = baseFare;
        this.ratePerKilometer = ratePerKilometer;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getServiceType() {
        return serviceType;
    }

    public double getBaseFare() {
        return baseFare;
    }

    public double getRatePerKilometer() {
        return ratePerKilometer;
    }
}

class Order {
    private String serviceType;
    private Driver driver;
    private String restaurantName;
    private String startLocation;
    private String destination;
    private double distance;
    private double fare;
    private String paymentMethod;

    public Order(String serviceType, Driver driver, String startLocation, String destination, double distance, double fare) {
        this.serviceType = serviceType;
        this.driver = driver;
        this.startLocation = startLocation;
        this.destination = destination;
        this.distance = distance;
        this.fare = fare;
    }

    public String getServiceType() {
        return serviceType;
    }

    public Driver getDriver() {
        return driver;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public String getDestination() {
        return destination;
    }

    public double getDistance() {
        return distance;
    }

    public double getFare() {
        return fare;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}

class Driver {
    private String name;
    private String licensePlate;

    public Driver(String name, String licensePlate) {
        this.name = name;
        this.licensePlate = licensePlate;
    }

    public String getName() {
        return name;
    }

    public String getLicensePlate() {
        return licensePlate;
    }
}
class Payment {
    private double amount;
    private String paymentMethod;

    public Payment(double amount, String paymentMethod) {
        this.amount = amount;
        this.paymentMethod = paymentMethod;
    }

    public double getAmount() {
        return amount;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }
}
```
Penerapan setiap prinsip SOLID pada kode GojekApp:

-Single Responsibility Principle (SRP):

Kelas GojekApp menangani logika utama aplikasi dan antarmuka pengguna.
Kelas Service, Order, dan Driver mewakili model data untuk layanan, pesanan, dan pengemudi, masing-masing.
Kelas Payment mewakili model data pembayaran.
Open/Closed Principle (OCP):

Kode ini menunjukkan beberapa tingkat ketaatan terhadap prinsip OCP. Misalnya, kelas Service, Order, dan Driver dibuat dengan atribut-atribut tertentu, dan menambahkan jenis layanan, pesanan, atau pengemudi baru tidak memerlukan modifikasi pada kelas-kelas yang sudah ada. Namun, bisa lebih baik dengan abstraksi yang lebih baik untuk sepenuhnya mematuhi OCP.

-Interface Segregation Principle (ISP):

Kode ini tidak memiliki antarmuka khusus yang didefinisikan, sehingga ISP tidak secara langsung berlaku dalam konteks ini. Namun, kode ini menggunakan antarmuka ActionListener untuk menangani peristiwa tombol.

-Dependency Inversion Principle (DIP):

Kelas GojekApp bergantung pada implementasi konkret dari layanan, pengemudi, dan pesanan, tetapi kode ini bisa ditingkatkan untuk bergantung pada abstraksi, mengurangi ketergantungan antar komponen.
Dependency injection bisa digunakan untuk memberikan dependensi yang diperlukan ke kelas GojekApp dan kelas lainnya, sehingga membuat kode lebih fleksibel dan dapat diuji.

## 4.Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
# decorator Pattern
```java
interface Order {
    double getFare();
    String getServiceType();
    Driver getDriver();
    String getDestination();
    double getDistance();
    String getRestaurantName();
    void setPaymentMethod(String paymentMethod);
    String getStartLocation(); // Add this method to the interface
}
// Kelas dasar untuk order
class BaseOrder implements Order {
    private String serviceType;
    private Driver driver;
    private String restaurantName;
    private String startLocation;
    private String destination;
    private double distance;
    private double fare;
    private String paymentMethod;

    public BaseOrder(String serviceType, Driver driver, String startLocation, String destination, double distance, double fare) {
        this.serviceType = serviceType;
        this.driver = driver;
        this.startLocation = startLocation;
        this.destination = destination;
        this.distance = distance;
        this.fare = fare;
    }

    public String getServiceType() {
        return serviceType;
    }

    public Driver getDriver() {
        return driver;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public String getDestination() {
        return destination;
    }

    public double getDistance() {
        return distance;
    }

    public double getFare() {
        return fare;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
// Decorator untuk promo
class PromoDecorator implements Order {
    private Order order;

    public PromoDecorator(Order order) {
        this.order = order;
    }

    @Override
    public double getFare() {
        return order.getFare();
    }

    @Override
    public String getServiceType() {
        return order.getServiceType();
    }

    @Override
    public Driver getDriver() {
        return order.getDriver();
    }

    @Override
    public String getStartLocation() {
        return order.getStartLocation();
    }

    @Override
    public String getDestination() {
        return order.getDestination();
    }

    @Override
    public double getDistance() {
        return order.getDistance();
    }
    @Override
    public void setPaymentMethod(String paymentMethod) {
        order.setPaymentMethod(paymentMethod);
    }
    @Override
    public String getRestaurantName() {
        return order.getRestaurantName();
    }
}

// Subclass dari PromoDecorator untuk memberikan diskon berdasarkan kode promo
class PromoOrderDecorator extends PromoDecorator {
    private double discount;

    public PromoOrderDecorator(Order order, double discount) {
        super(order);
        this.discount = discount;
    }

    @Override
    public double getFare() {
        // Hitung total harga setelah promo
        double totalFare = super.getFare();
        return totalFare - (totalFare * discount);
    }
}

```
# 5.Mampu menunjukkan dan menjelaskan konektivitas ke database
![](https://gitlab.com/Fadilahali/basis-data/-/raw/main/UAS/conect.gif)
# 6.Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya
![](https://gitlab.com/Fadilahali/basis-data/-/raw/main/UAS/webse.gif)
# 7.Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
![](https://gitlab.com/tubes-pbo1/tubes/-/raw/main/interfCW.gif)
# 8.Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
![](https://gitlab.com/tubes-pbo1/tubes/-/raw/main/conut.gif)
# 9.Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
# 10.BONUS !!! Mendemonstrasikan penggunaan Machine Learning
