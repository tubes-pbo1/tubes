## USE CASE



Use Case | bisa | Nilai Priorita|
---|---|---
user | dapat mendaftar Akun |100
user | login menggunkan akun yang terdaftar | 100
user | dapat memilih  menu yang di inginkan | 100
user | dapat memilih menu goride | 100
user | memilih driver yang tersedia di lokasi terdekat | 80
user | bisa menuntukan titik jemput  | 80
user | bisa memilih kendaraan yang tersedia | 90
user | user bisa order dan tunggu driver menjemput | 100
user | user dapat melihat saldo gopay  | 80
user | user dapat menambahkan vocher diskon | 40
user | bisa memilih menu Gofood | 100
user | user memasukan lokasi  | 80
user | memilih  item yang di inginkan | 80
user | user memasukan item yang di pilih ke keranjang | 80
user | user meng cekout item yang di pilih | 80
user | driver siap mengantarkan item | 80
user | user dapat memilih Gocar  |80 
user | user dapat memilih lokasi jemput Gocar | 80
user | user dapat Memili Gocar yang Ready di sekitar | 80
user | user bisa toup up Gopay |80 
user | user dapat bayar mengunakan Gopay | 80
user | user dapat memilih menu Gosend|80
user | user memilih pengiriman ke dalam atau luar kota |80
user | user dapat measuka data diri pengirim |70
user | user dapat memilih menu Gomart | 80
user | user dapat memilih barang atau makanan yang akan di beli | 70
user | user melihat pesanan yang sudah di lakukan di Gomart |70
user | user dapat melihat promo promo Gojek | 80
user | user dapat melihat promo yang di miliki | 80
user | user dapat menggunakan Promo yang tersedia |70
user | user dapat melakukan chat dengan Driver | 80
user | user dapat memilih Gotagihan | 80
user | di Go tagihan user bisa mau bayar tagihan apa | 60
user | dapat Isi ulang segala prabayar | 60
user | dapat memesan kreta di Gotransit |70
user | dapat membeli segala jenis Obat di Gomed | 70
user | dapat bergabung dengan Goclub | 60
user | dapat dapat memesan jasa angkut barang di GoBox|70
user | dapat membeli segala tiket Di Gotix | 70
user | dapat nonton tayangan eksklusif dari Goplay | 60
manejemen| dapat melihat seberapa banyak user memakai kode Promo|40
menejemen| dapat melihat seberapa banyak Produk gofod dan gomart terjual|40
menejemen | dapat melihat statistik aplikasi gojek | 40
menejemen | dapat melihat banyaknya pengguna | 40



## Diagram Class
```mermaid
classDiagram
AppGojek --* Akun
AppGojek : +ArrayList
AppGojek --* Goride
AppGojek --* Gofood
AppGojek --* Gomart
AppGojek --* Gotix
AppGojek --* Gocar
AppGojek --* GoTagihan
AppGojek --* GoTransit
AppGojek --* Gomed
AppGojek --* Gosend
AppGojek --* Gobox

Class Akun 
<<Abstract>> Akun 
Akun : +String Nama
Akun : +String Nomer Telpon
Akun : + intorductiomyself()
Akun --* User
User : +String Email
User : +String Pasword
User : +Login()
Akun --* Driver 
Driver : +String PlatNomber
Driver : +Login()
Class Goride
Goride : +Double Distance
Goride : +String Lokasi Awal
Goride : +String LokasiTujuan
Goride : +LokasiAwal()
Goride : +LokasiTujuan()
Goride --|> Order

Class Order
Order : +Double Distance
Order : +Double Price
Order : + String IteName
Order : +getPrice()

Class Gofood
Gofood --|> Order
Gofood : +String MenuMakanan
Gofood : +PesanMakanan()

Class Gomart
Gomart --|> Order

Class Payment
Payment --|> Order
Payment : +Double Amount
Payment : +String PaymentMethode
Payment : +setaAmount
Payment : +setPaymentMethode



```
