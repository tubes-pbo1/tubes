### mampu mendemonstrasikan penggunaan Encapsulation secara tepat

```java 
class Payment {
    private double amount;
    private String paymentMethod;

    public Payment(double amount, String paymentMethod) {
        this.amount = amount;
        this.paymentMethod = paymentMethod;
    }

    public double getAmount() {
        return amount;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
//untuk pemesanan ojek online
class GoRide {

    private double distance;
    private String lokasiAwal;
    private String lokasiTujuan;

    public GoRide() {
        this.distance = 0.0;
        this.lokasiAwal= lokasiAwal;
        this.lokasiTujuan = lokasiTujuan;

    }

    public String getLokasiTujuan() {
        return lokasiTujuan;
    }

    public void setLokasiTujuan(String lokasiTujuan) {
        this.lokasiTujuan = lokasiTujuan;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
    public String getlokasiAwal (){
        return lokasiAwal;
    }
    public void setlokasiAwal(String lokasiAwal){
        this.lokasiAwal = lokasiAwal;
    }

}

```
