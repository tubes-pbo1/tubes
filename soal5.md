### Mampu mendemonstrasikan penggunaan Abstraction secara tepat 
```java
abstract class Akun {
    private String name;            // menyimpan nama akun
    private String NomerTelpon;     // menyimpan nomor telepon akun

    // constructor kelas Akun
    public Akun(String name, String NomerTelpon) {
        this.name = name;
        this.NomerTelpon = NomerTelpon;
    }

    // getter untuk nama akun
    public String getName() {
        return name;
    }

    // getter untuk nomor telepon akun
    public String getNomerTelpon() {
        return NomerTelpon;
    }

    // method abstrak introduceYourself
    public abstract void introduceYourself();
}

// Pembuatan kelas abstrak User yang merupakan subclass dari kelas Akun
abstract class User extends Akun {
    private String email;           // menyimpan alamat email akun
    private String password;        // menyimpan password akun

    // constructor kelas User
    public User(String name, String NomerTelpon, String email, String password) {
        super (name, NomerTelpon);
        this.email = email;
        this.password = password;
    }

    // getter untuk alamat email akun
    public String getEmail() {
        return email;
    }

    // getter untuk password akun
    public String getPassword() {
        return password;
    }

    // method abstrak login
    public abstract void login();
}
```
