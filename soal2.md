### Mampu menjelaskan algoritma dari solusi yang dibuat 
```java 
//proses bisnis dari program Gojek yang telah diberikan:

//    1. User Authentication

//     User memasukkan nama, password, dan nomor HP.
//    Sistem memeriksa kecocokan data pengguna dengan data yang ada di sistem Gojek.
//    Jika data pengguna sesuai, maka sistem menampilkan pesan "Selamat Datang Di Gojek" dan menu utama.
//    Jika data pengguna tidak sesuai, maka sistem meminta pengguna untuk memasukkan data ulang atau mendaftar akun baru.

//   2. Gojek Service
//   Pengguna memilih menu Gojek.
//   Memilih driver yang ready di sekitar
//   Pengguna memasukkan jarak tempuh.
//   Sistem menghitung total harga dan menampilkan detail pesanan.
//   Sistem menampilkan pesan "Pembayaran sebesar [jumlah uang] berhasil dilakukan dengan [metode pembayaran]."

// 3. GoFood Service
//  Pengguna memilih menu GoFood.
//  Sistem menampilkan daftar menu makanan yang tersedia.
//  Pengguna memilih menu makanan yang ingin dipesan.
//  Pengguna memasukkan jarak tempuh.
//  Sistem menghitung total harga dan menampilkan detail pesanan.
//  Pengguna memilih metode pembayaran.
//  Sistem memproses pembayaran.
//  Sistem menampilkan pesan "Pembayaran sebesar [jumlah uang] berhasil dilakukan dengan [metode pembayaran]."
//  Order

// order history
// Pengguna memilih menu Order History.
// Sistem menampilkan riwayat pesanan pengguna, termasuk detail pesanan dan informasi pembayaran.

// Payment
// Pengguna memilih menu Payment.
// Sistem menampilkan pilihan metode pembayaran.
// Pengguna memilih metode pembayaran.
// Pengguna memasukkan jumlah uang yang ingin dibayarkan.
// Sistem memproses pembayaran.
// Sistem menampilkan pesan "Pembayaran sebesar [jumlah uang] berhasil dilakukan dengan [metode pembayaran]."

import java.util.ArrayList;
import java.util.Scanner;

// Pembuatan kelas abstrak Akun
abstract class Akun {
    private String name;            // menyimpan nama akun
    private String NomerTelpon;     // menyimpan nomor telepon akun

    // constructor kelas Akun
    public Akun(String name, String NomerTelpon) {
        this.name = name;
        this.NomerTelpon = NomerTelpon;
    }

    // getter untuk nama akun
    public String getName() {
        return name;
    }

    // getter untuk nomor telepon akun
    public String getNomerTelpon() {
        return NomerTelpon;
    }

    // method abstrak introduceYourself
    public abstract void introduceYourself();
}

// Pembuatan kelas abstrak User yang merupakan subclass dari kelas Akun
abstract class User extends Akun {
    private String email;           // menyimpan alamat email akun
    private String password;        // menyimpan password akun

    // constructor kelas User
    public User(String name, String NomerTelpon, String email, String password) {
        super (name, NomerTelpon);
        this.email = email;
        this.password = password;
    }

    // getter untuk alamat email akun
    public String getEmail() {
        return email;
    }

    // getter untuk password akun
    public String getPassword() {
        return password;
    }

    // method abstrak login
    public abstract void login();
}

// Pembuatan kelas abstrak Driver yang merupakan subclass dari kelas Akun
abstract class Driver extends Akun {
    private String PlatNomber;      // menyimpan nomor plat kendaraan driver

    // constructor kelas Driver
    public Driver(String name, String NomerTelpon, String Platnomber) {
        super(name, NomerTelpon);
        this.PlatNomber = Platnomber;
    }

    // getter untuk nomor plat kendaraan driver
    public String getPlatNomber() {
        return PlatNomber;
    }

    // method abstrak login
    public abstract void login();
}

// Pembuatan kelas Order untuk memesan makanan
class Order {
    private double jarakTempuh;     // menyimpan jarak tempuh pesanan
    private double Price;           // menyimpan harga pesanan
    private String itemName;        // menyimpan nama makanan pesanan

    // constructor kelas Order
    public Order(String itemName, double jarakTempuh) {
        this.itemName = itemName;
        this.jarakTempuh = jarakTempuh;
    }

    // getter untuk harga pesanan
    public double getPrice() {
        return Price;
    }

    // method untuk menampilkan pesanan
    public void display() {
        System.out.println("Makanan: " + itemName);
        System.out.println("Jarak Tempuh: " + jarakTempuh + " km");
    }
}
// Pembuatan kelas Payment untuk metode pembayaran

class Payment {
    private double amount;
    private String paymentMethod;

    public Payment(double amount, String paymentMethod) {
        this.amount = amount;
        this.paymentMethod = paymentMethod;
    }

    public double getAmount() {
        return amount;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
//untuk pemesanan ojek online
class GoRide {

    private double distance;
    private String lokasiAwal;
    private String lokasiTujuan;

    public GoRide() {
        this.distance = 0.0;
        this.lokasiAwal= lokasiAwal;
        this.lokasiTujuan = lokasiTujuan;

    }

    public String getLokasiTujuan() {
        return lokasiTujuan;
    }

    public void setLokasiTujuan(String lokasiTujuan) {
        this.lokasiTujuan = lokasiTujuan;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
    public String getlokasiAwal (){
        return lokasiAwal;
    }
    public void setlokasiAwal(String lokasiAwal){
        this.lokasiAwal = lokasiAwal;
    }

}


//class gofood
class GoFood {
    private ArrayList<String> menuMakanan;

    public GoFood() {
        //pilihan menu makananan
        this.menuMakanan = new ArrayList<String>();
        this.menuMakanan.add("Nasi Goreng");
        this.menuMakanan.add("Mie Ayam");
        this.menuMakanan.add("Sate Ayam");
        this.menuMakanan.add("Soto Ayam");
    }

    public ArrayList<String> getMenuMakanan() {
        return this.menuMakanan;
    }

    public void pesanMakanan(String menu, double jarakTempuh) {
        Order order = new Order(menu, jarakTempuh);
        order.display();
        double Price = order.getPrice();
        System.out.println("Total Harga: " + Price);
    }
}
//class Utamanya
public class AppGojek {
    //Implementasi class user dan driver
    private static ArrayList<User> users = new ArrayList<User>(); //mendefinisikan ArrayList users yang akan menyimpan objek-objek User
    private static ArrayList<Driver> drivers = new ArrayList<Driver>(); //mendefinisikan ArrayList drivers yang akan menyimpan objek-objek Driver
    private static User currentUser = null; //currentUser akan digunakan untuk menyimpan User yang sedang login saat ini
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); //Membuat objek Scanner untuk membaca input dari pengguna
        GoRide ride = new GoRide();
        ride.setlokasiAwal("Jalan Sudirman");
        ride.setLokasiTujuan("Jalan Thamrin");

        //Implimentasi dari User driver yang telah mendaftar ke gojek
        // Menambahkan beberapa driver ke dalam arraylist drivers
        ArrayList<String> drivers = new ArrayList<String>();
        drivers.add("Budi = Yamaha Nmax");
        drivers.add("Cahyo = Honda Beat");
        drivers.add("Dodi = Honda Sufra X");
        drivers.add("santo = Vespa Matic");



        // Menu Login dan mendaftar
        while (true) { // melakukan loop terus menerus hingga user memilih keluar
            System.out.println("Selamat Datang di Gojek");
            System.out.println("1. Login");
            System.out.println("2. Daftar");
            System.out.println("3. keluar");
            System.out.print("Pilih menu: ");
            int menu = scanner.nextInt(); // membaca pilihan menu dari pengguna
            scanner.nextLine(); // membersihkan buffer setelah membaca input integer

            if (menu == 1) { // jika pengguna memilih opsi 1, yaitu Login
                System.out.print("Email: ");
                String email = scanner.nextLine(); //membaca email dari pengguna
                System.out.print("Password: ");
                String password = scanner.nextLine(); //membaca password dari pengguna

                boolean userFound = false;
                for (User user : users) { //looping melalui setiap User dalam ArrayList users
                    if (user.getEmail().equals(email)&& user.getPassword().equals(password)) { //jika ditemukan User dengan email dan password yang sesuai
                        currentUser = user; //set currentUser menjadi user yang sedang login
                        System.out.println("Selamat Datang, " + user.getName()); //menampilkan pesan selamat datang
                        userFound = true; //mengubah variabel userFound menjadi true, menandakan User berhasil ditemukan
                        break; //keluar dari loop
                    }
                }

                if (!userFound) { //jika variabel userFound masih false, menandakan tidak ada User yang ditemukan
                    System.out.println("Email atau password salah."); //menampilkan pesan bahwa email atau password salah
                }

            } else if (menu == 2) { // jika pengguna memilih opsi 2, yaitu Daftar
                System.out.print("Nama: ");
                String name = scanner.nextLine(); //membaca nama dari pengguna
                System.out.print("Nomer Telpon: ");
                String nomerTelpon = scanner.nextLine(); //membaca nomor telepon dari pengguna
                System.out.print("Email: ");
                String email = scanner.nextLine(); //membaca email dari pengguna
                System.out.print("Password: ");
                String password = scanner.nextLine(); //membaca password dari pengguna

                User user1 = new User(name, nomerTelpon, email, password) { // Membuat objek user baru dan menambahkannya ke dalam arraylist users
                    @Override
                    public void login() {}
                    @Override
                    public void introduceYourself() {}
                };
                users.add(user1);//menambahkan user yang baru dibuat ke ArrayList users
                currentUser = user1;//set currentUser menjadi user1 yang baru saja mendaftar
                System.out.println("Akun berhasil dibuat. Selamat Datang, " + name);
            } else if (menu == 3) { //jika pengguna memilih opsi 3, yaitu Keluar
                break; //keluar dari loop dan program berakhir
            } else { //jika pengguna memilih opsi yang tidak valid
                System.out.println("Menu tidak valid."); //menampilkan pesan bahwa menu tidak valid
            }
        }

        // Menu Utama setelah login atau mendaftar
        int menuChoice;
        do {
            System.out.println("====================");
            System.out.println("==== gojek-app =====");
            System.out.println("====================");
            System.out.println("1. Profile User");
            System.out.println("2. Order Goride");
            System.out.println("3. Order Gofood");
            System.out.println("4. order history");
            System.out.println("5. Payment");
            System.out.println("6. Exit");
            System.out.print("Enter your choice: ");
            menuChoice = scanner.nextInt();

            switch (menuChoice) {
                case 1:
                    if (currentUser != null) {
                        System.out.println("Nama: " + currentUser.getName());
                        System.out.println("Email: " + currentUser.getEmail());
                        currentUser.introduceYourself();
                    } else {
                        System.out.println("Anda belum login.");
                    }
                    break;
                case 2:

                    System.out.print("Distance (in km): ");
                    double distance = scanner.nextDouble();

                    if (distance > 100) {
                        System.out.println("Error: Distance is too far.");
                        break;
                    }

                    double price = distance * 5000; // harga per kilometer 5000

                    if (price > 100000) { // batasi harga maksimal
                        System.out.println("Error: Price is too high.");
                        break;
                    }

                    System.out.println("Order Distance: " + distance + " km");
                    System.out.println("Order Price: Rp " + price);
                    System.out.println("Lokasi Awal: " + ride.getlokasiAwal());
                    System.out.println("Lokasi Tujuan: " + ride.getLokasiTujuan());

                    // Memilih driver
                    System.out.println("=========================");
                    System.out.println("==== Driver Goride =======");
                    System.out.println("=========================");
                    System.out.println("Available Drivers:");
                    System.out.println("Available Drivers:");
                    for (int i = 0; i < drivers.size(); i++) {
                        System.out.println(i + 1 + ". " + drivers.get(i));
                    }
                    System.out.print("Pilih driver (1-" + drivers.size() + "): ");
                    int driverChoice = scanner.nextInt();
                    if (driverChoice >= 1 && driverChoice <= drivers.size()) {
                        String selectedDriver = drivers.get(driverChoice - 1);
                        System.out.println("driver: " + selectedDriver);
                    } else {
                        System.out.println("Pilihan driver tidak valid.");
                    }
                {

                }
                break;
                case 3:
                    // code implementasi Gofood
                    GoFood goFood = new GoFood();
                    ArrayList<String> menuMakanan = goFood.getMenuMakanan();
                    System.out.println("Menu Makanan:");
                    for (int i = 0; i < menuMakanan.size(); i++) {
                        System.out.println((i + 1) + ". " + menuMakanan.get(i));
                    }
                    System.out.print("Enter the food you want to order: ");
                    scanner.nextLine();
                    String food = scanner.nextLine();
                    System.out.print("Enter the distance (in km): ");
                    double distance2 = scanner.nextDouble();
                    goFood.pesanMakanan(food, distance2);
                    break;
                case 4:
                    System.out.println("Pilih Jenis Order Histori");
                    System.out.println("1.Goride :");
                    System.out.println("2.Gofood :");
                    System.out.println("3.Selesai :");
                    System.out.print("pilih :");
                    int orderHistoryChoice = scanner.nextInt();
                    scanner.nextLine();


                    switch (orderHistoryChoice){
                        case 1:
                            System.out.println("=====================");
                            System.out.println("Histori Goride Anda :");
                            System.out.println("Username :"+currentUser.getName());
                            System.out.println("Nomer Telpon :"+currentUser.getNomerTelpon());
                            System.out.println("Lokasi Awal: " + ride.getlokasiAwal());
                            System.out.println("Lokasi Tujuan: " + ride.getLokasiTujuan());

                            break;
                        case 2:
                            System.out.println("Histori GoFood anda :");
                            System.out.println("nama    :"+ currentUser.getName());
                            System.out.println("nomer   :"+ currentUser.getNomerTelpon());
                            System.out.println("Makananan : Nasi Goreng ");
                            break;
                        case 3:
                            System.out.println("Terimaksih Telah Order");
                            break;
                        default:
                            System.out.println("Invalid choice. Please try again.");
                            break;
                    }
                    break;


                case 5:
                    // Implementasi class peyament dan menu dan metode payment
                    System.out.println("========================");
                    System.out.println("Pilih metode pembayaran:");
                    System.out.println("========================");
                    System.out.println("1. Cash");
                    System.out.println("2. Dana");
                    System.out.println("3. Gopay");
                    System.out.println("4. Kartu Debit");
                    System.out.println("5. kartu kredit");
                    System.out.println("Pilih Metode Pembayaran :");
                    int paymentOption = scanner.nextInt();
                    scanner.nextLine();
                    if (paymentOption < 1 || paymentOption > 5) {
                        System.out.println("Pilihan metode pembayaran tidak tersedia.");
                        break;
                    }
                    System.out.print("Masukkan jumlah uang yang ingin dibayarkan: ");
                    double amount = scanner.nextDouble();
                    Payment payment = new Payment(amount, "");
                    switch (paymentOption) {
                        case 1:
                            payment.setPaymentMethod("cash");
                            break;
                        case 2:
                            payment.setPaymentMethod("Dana");
                            break;
                        case 3:
                            payment.setPaymentMethod("Gopay");
                            break;
                        case 4:
                            payment.setPaymentMethod("kartu Debit");
                            break;
                        case 5:
                            payment.setPaymentMethod("kartu Kredit");
                    }
                    System.out.println("Pembayaran sebesar " + payment.getAmount() + " berhasil dilakukan dengan " + payment.getPaymentMethod() + ".");
                    break;

                case 6:
                    System.out.println("Exiting...");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }
        }
        while (menuChoice != 6);
    }
}

```
